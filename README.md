# IUTInfoDiscordBot
Code d'un bot codé pour un Discord du département informatique d'un IUT.

## How to use ?
First, you will need to have Ruby on your computer. This bot was created with the 2.5.1 version. I don't know if it will
works on older or newer versions. You can download and install Ruby for your runtime on the Ruby website (https://www.ruby-lang.org/en/downloads/).
You will need bundler, so use ```gem install bundler``` in the repo directory, after cloning.

Clone the repo
```git clone https://github.com/dracoctix/IUTInfoDiscordBot.git```, then ```bundle install --deployment``` the module.
You need to create a Discord bot on Developer portal (https://discordapp.com/developers/applications/), then get the token .
Copy the file ```credentials_example.rb``` to ```credentials.rb```.
In this file, you will set the value of ```BOT_TOKEN``` as the bot token, and ```BOT_OWNER``` as your Discord user ID.
You can obtain it by the Discord developer mode, with a right clik on your username.
The third constant, ```COMMAND_PREFIX``` is the prefix for commands. The default value is ```!```, but you can change it
if you need, for example, to avoid conflicts.
Then run the app with ```ruby run.rb```. The bot will run and use the bot account. You can use all of the boss features.

There is few commands. You, and only yourself can use the `!eval` command to execute ruby code.
For security reasons, only the bot owner can use this command.
### Other commands
* ```!help``` List all commands availables to every members.
* ```!rejoindre <role>``` The user can use this command to have the mentioned role, if it's allowed.
* ```!quitter <role>``` The user can use this command to lose the mentioned role, if it's allowed.
* ```!liste``` List all roles that users can join with the two commands listed below.
* ```!ajouter <role>``` The administrator can use this command to allow to join or quit the mentioned role with a command.
* ```!retirer <role>``` The administrator can use this command to disallow to join or quit the mentioned role with a command.

**Note :** The two last commands need to have the role management permissions to be used.
The people who doesn't have this permission will have an error message.

## Final note

If there is a bug during bot execution, please contact me, so I can fix the bug, or prevent it. Thanks for reading !
